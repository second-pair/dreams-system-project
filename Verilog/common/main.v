module main (clk, bbClk, bbRst, bbIn, bbOut, srvOverride, ovrSrvPos, ovrSrvEn, pwmOut, colourI2CIn, colourI2COut, brkbmIn, brkbmOut, ledOut, segOut);
	//  NOTE:  pulseCount is 12-bits, for auto-reset after 16ms of pulses
	//  This code is adapted from code found on 'http://www.fpga4fun.com/'

	//  General
	input clk;  //  50MHz clock
	//  7-Segment & LEDs
	output [27:0] segOut;
	output [2:0] ledOut;
	//  Bit Banging
	parameter bbAddrSize = 2;
	input bbClk, bbRst, bbIn;  //  bbClk is driven by the MBED
	output bbOut;
	//  Servo
	parameter srvCnt = 4;  //  Total number of servos being controlled by the FPGA
	input srvOverride;  //  Maintenance option to override input server positions - controlled by onboard switches
	wire [7:0] newSrvPos;  //  Desired 7-bit servo position
	input [7:0] ovrSrvPos;  //  Override server position
	wire [srvCnt-1:0] srvEn;  //  Which servos to update with srvPos
	input [srvCnt-1:0] ovrSrvEn;  //  Override Servo Enable
	output [srvCnt-1:0] pwmOut;  //  Servo PWM output, one bit per servo
	wire [7:0] corrNewSrvPos;
	wire [srvCnt-1:0] corrSrvEn;
	//  Colour Sensor
	parameter colourSnsrCnt = 1;
	input [(colourSnsrCnt*2)-1:0] colourI2CIn;
	output [(colourSnsrCnt*2)-1:0] colourI2COut;
	//  Breakbeam Sensor
	parameter brkbmCnt = 2;
	input [brkbmCnt-1:0] brkbmIn;
	output [brkbmCnt-1:0] brkbmOut;
	//  Internals
	wire [8*srvCnt-1:0] allSrvPos;
	wire [11:0] pulseCount;

	//  General Assignments
	assign colourI2COut = colourI2CIn;  //  Colour sensor passthrough
	assign brkbmOut = brkbmIn;  //  Breakbeam passthrough
	assign corrNewSrvPos = srvOverride ? ovrSrvPos : newSrvPos;  //  Choose between input and override values
	assign corrSrvEn = srvOverride ? ~ovrSrvEn : srvEn;  //  Choose between input and override values
	assign ledOut [0] = srvOverride;  //  LED indicator for servo override mode
	//assign ledOut [2:1] = srvOverride ? ~brkbmIn : 2'b00;  //  LED indicators for breakbeam detection
	assign ledOut [1] = brkbmIn [0];
	assign ledOut [2] = brkbmIn [1];

	//  Module Instantiation
	timeKeeper tk0 (.clk (clk), .pulseCount (pulseCount));  //  Fully Simulated; Fully Verified
	servoStorage #(.srvCnt (srvCnt)) ss0 (.clk (clk), .newSrvPos (corrNewSrvPos), .srvEn (corrSrvEn), .srvPos (allSrvPos));  //  Partially Simulated; Partially Verified
	servoPulseGen #(.srvCnt (srvCnt)) spg0 (.pulseCount (pulseCount), .srvPos (allSrvPos), .pwmOut (pwmOut));  //  Partially Simulated; Partially Verified
	sevenSeg s70 (.binIn (corrNewSrvPos), .segOut (segOut));  //  Fully Simulated; Partially Verified
	bitBang #(.srvCnt (srvCnt), .brkbmCnt (brkbmCnt), .addrSize(bbAddrSize)) bb0 (.clk (bbClk), .bbNRst (bbRst), .bbIn (bbIn), .bbOut (bbOut), .brkbmData (brkbmIn), .srvPosData (newSrvPos), .srvEnData (srvEn));
endmodule

/*
-=-  Wires  -=-
-=>  FPGA Pin Assignments
fpga.gpio0.11 = VCC5
fpga.gpio0.12 = GND
fpga.gpio0.29 = VCC33
fpga.gpio0.1-10 = GPIO
fpga.gpio0.13-26 = GPIO
fpga.gpio0.28-40 = GPIO
fpga.gpio1.11 = VCC5
fpga.gpio1.12 = GND
fpga.gpio1.29 = VCC33
fpga.gpio1.1-10 = GPIO
fpga.gpio1.13-26 = GPIO
fpga.gpio1.28-40 = GPIO

-=>  Required Input
- Servos
8-bit position
n-bit enable
- Colour Sensor
2n-bit I2C data
- Breakbeam Sensors
n-bit sense

-=>  Required Output
- Servos
n-bit PWM output
- Colour Sensor
2n-bit I2C data
- Breakbeam Sensors
n-bit sense

-=>  Suggested Pin IO
- To Devices
fpga.gpio0.1-10 = servo PWM output
fpga.gpio0.13-18 = colour I2C data
fpga.gpio0.19-26 = breakbeam sense

- To MBED
fpga.gpio1.1 = bit bang clock - driven by MBED
fpga.gpio.1.2 = bit bang reset
fpga.gpio1.3 = bit bang receive
fpga.gpio1.4 = bit bang send
fpga.gpio1.5-6 = breakbeam passthrough
fpga.gpio1.7-8 = colour I2C passthrough

- Old To MBED
fpga.gpio1.1-10 = servo enable
fpga.gpio1.13-20 = servo position
fpga.gpio1.21-26 = colour I2C data
fpga.gpio1.28-35 = breakbeam sense
*/
