module servoPulseGen (pulseCount, srvPos, pwmOut);
	//  TODO:  Figure out how to do the always block, with n stages
	//  NOTE:  pulseCount is 12-bits, for auto-reset after 16ms of pulses
	//  Generates PWM waveform for each servo

	//  Parameters
	parameter srvCnt = 1;
	//  Inputs
	input [11:0] pulseCount;
	input [8*srvCnt-1:0] srvPos;
	//  Outputs
	output [srvCnt-1:0] pwmOut;  //  Servo PWM output, one bit per servo
	reg [srvCnt-1:0] pwmOut;

	//integer i;
	always @ (pulseCount)
	begin
		/*
		for (i = 0; i >= srvCnt; i = i + 1)
		begin
			pwmOut [i] <= (pulseCount < (256 + srvPos [((i+1)*8)-1:i*8]));
		end*/
		/*
		pwmOut [0] <= (pulseCount < {1'b1 + srvPos [7:0]});
		pwmOut [1] <= (pulseCount < {1'b1 + srvPos [15:8]});
		pwmOut [2] <= (pulseCount < {1'b1 + srvPos [23:16]});
		pwmOut [3] <= (pulseCount < {1'b1 + srvPos [31:24]});
		*/
		pwmOut [0] <= (pulseCount < (256 + srvPos [7:0]));
		pwmOut [1] <= (pulseCount < (256 + srvPos [15:8]));
		pwmOut [2] <= (pulseCount < (256 + srvPos [23:16]));
		pwmOut [3] <= (pulseCount < (256 + srvPos [31:24]));
	end

	/*reg [7:0] c;
	generate
	    always @ (pulseCount)
		begin
	        for (c = 0; c < srvCnt; c = c + 1)
			begin
				//  output = bool of (has pulseCount reached 1ms (256 ticks) + desired servoPos?)
				pwmOut [c] <= (pulseCount < {1'b1 + srvPos [c]});
	        end
	    end
	endgenerate*/
endmodule
