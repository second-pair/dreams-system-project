module timeKeeper (clk, pulseCount);
	//  NOTE:  pulseCount is 12-bits, for auto-reset after 16ms of pulses
	//  Divides the main clock into a 1/256 ms clock

	//  Parameters
	parameter pulsePeriod = 195;  //  Quartus 7 doesn't like parameters referenced outside of the current module
	//  Inputs
	input clk;
	//  Outputs
	output [11:0] pulseCount;  //  Used to track how many 1/256 ms ticks have elapsed
	reg [11:0] pulseCount;  //  Auto-resets when overflows at 16384
	//  Internals
	reg [7:0] clkCount;  //  Used to track how far through 1/256 ms we are

	always @ (posedge clk)
	begin
		if (clkCount == pulsePeriod - 2)  //  Check if time for a tick
		//if (clkCount == digitalEx2_14.pulsePeriod - 2)
		begin
			//  Increment pulseCount each tick
			pulseCount <= pulseCount + 1'b1;
			clkCount <= 0;
		end
		else
			clkCount <= clkCount + 1'b1;  //  reset clkCount when 1 tick, else continue counting
	end
endmodule
