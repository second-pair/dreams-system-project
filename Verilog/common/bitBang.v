module bitBang (clk, bbNRst, bbIn, bbOut, brkbmData, srvPosData, srvEnData);
	parameter srvCnt = 4;
	parameter brkbmCnt = 2;
	parameter addrSize = 2;
	reg [7:0] srvDataLen = 8'd8 + 8'd8 * (srvCnt / 8'd8 + 8'd1);  //  Local adaption of srvCnt
	reg [7:0] brkbmDataLen = 8'd8 * (brkbmCnt / 8'd8 + 8'd1);  //  Local adaption of brkbmCnt
	input clk, bbNRst, bbIn;
	output reg bbOut;
	reg [7:0] currStage = 2'd0;  //  Stage of Data IO
	reg [1:0] currAddr;  //  Stores sent address as it is built
	reg [7:0] currData;  //  Stores sent data as it is built

	input [brkbmCnt-1:0] brkbmData;  //  Breakbeam data to send to MBED
	output reg [7:0] srvPosData;  //  Servo data received from MBED
	output reg [srvCnt-1:0] srvEnData;

	//  Init Part

	//  Main Part
	always @ (posedge clk)
	begin
		if (bbNRst == 1'b0)
		begin
			currStage = 8'b0;
			currAddr = 2'b0;
			currData = 8'b0;
			//srvEnData = 8'b0;
			//currData = 8'b0;
		end
		else
		begin
			//  Check if there's an input
			if (currStage == 0)//0
			begin
				if (bbIn) currStage = currStage + 1'b1;
			end

			//  Determine what's sent/received
			else if (currStage < addrSize)//1
			begin
				currAddr [currStage-1] = bbIn;
				srvEnData = 8'b0;
				//currData = 8'b0;
				currStage = currStage + 1'b1;
			end
			
			//  Jump to relevant code
			else if (currStage == addrSize)//2
			begin
				currAddr [currStage-1] = bbIn;
				if (currAddr == 2'd1)
					currStage = addrSize + brkbmDataLen + 8'd1;
				else if (currAddr == 2'd2)
					currStage = addrSize + 8'd1;
			end

			/*
			else if (currStage == 3)
			begin
				case (currAddr)
				2'd1:
					currStage = 8'd5 + brkbmDataLen;
				2'd2:
					currStage = 4;
				default:
					currStage = 0;
				endcase
				currStage = currStage + 1'b1;
			end*/

			//  Send breakbeam data
			else if (currStage < addrSize + brkbmDataLen)//3-9
			begin
				if (currStage <= addrSize + brkbmCnt)
					bbOut = brkbmData [currStage - addrSize - 1];
				else
					bbOut = 1'b0;
				currStage = currStage + 1'b1;
			end

			//  End of sending breakbeam data
			else if (currStage == addrSize + brkbmDataLen)//10
			begin
				if (currStage <= addrSize + brkbmCnt)
					bbOut = brkbmData [currStage - addrSize - 1];
				else
					bbOut = 1'b0;
				currStage = 8'b0;
			end

			//  Receive servo position data (guaranteed 8-bits)
			else if (currStage < addrSize + 8 + brkbmDataLen)//11-17
			begin
				currData [currStage - addrSize - brkbmDataLen - 1] = bbIn;
				currStage = currStage + 1'b1;
			end

			//  End of receiving servo position data
			else if (currStage == addrSize + 8 + brkbmDataLen)//18
			begin
				currData [currStage - addrSize - brkbmDataLen - 1] = bbIn;
				srvPosData = currData;  //  Assign received data to srvPosData register
				//currData = 8'b0;
				currStage = currStage + 1'b1;
			end

			//  Receive servo enable data
			else if (currStage < addrSize + brkbmDataLen + srvDataLen)//19-25
			begin
				if (currStage <= srvCnt + addrSize + brkbmDataLen + 8)  //  Only assign as many bits as are expected
					srvEnData [currStage - addrSize - brkbmDataLen - 8 - 1] = bbIn;
					//currData [currStage - addrSize - brkbmDataLen - 8 - 1] = bbIn;
				currStage = currStage + 1'b1;
			end

			//  End of receiving servo enable data
			else if (currStage == addrSize + brkbmDataLen + srvDataLen)//26
			begin
				if (currStage <= srvCnt + addrSize + brkbmDataLen + 8)  //  Only assign as many bits as are expected
					srvEnData [currStage - addrSize - brkbmDataLen - 8 - 1] = bbIn;
					//currData [currStage - addrSize - brkbmDataLen - 8 - 1] = bbIn;
				//srvEnData = currData;
				//currData = 8'b0;
				currStage = 8'b0;
			end

			//  Handle extraneous cases
			else
				currStage = 8'b0;
				
		end
	end
	/*
	always @ (addr, bbDataIn)
	begin
		case (currStage)
		2'd0:
			if (addr == 2'd0)
				currStage = 2'd0;
			else if (addr == 2'd1)  //  Servos
				currStage = 2'd1;
			else if (addr == 2'd2)  //  Breakbeam
				currStage = 2'd1;
			else
				currStage = 2'd0;
		2'd1:
			if (addr == 2'd1)
			begin
				currStage = 2'd2;
				srvPosData = bbDataIn;
			end
			else if (addr == 2'd2)
			begin
				currStage = 2'd0;
				bbDataOut = brkbmData;
			end
		2'd2:
			if (addr == 2'd1)
			begin
				currStage = 2'd0;
				srvEnData = bbDataIn [srvCnt-1:0];
			end
		endcase
	end*/
endmodule
