module servoStorage (clk, newSrvPos, srvEn, srvPos);
	//  TODO:  Figure out how to do the always block, with n stages
	//  Stores required servo posiitonal data at all times
	//  Updates requested servo position as required

	//  Parameters
	parameter srvCnt = 1;
	//  Inputs
	input clk;
	input [7:0] newSrvPos;  //  The desired position to update some servos with
	input [srvCnt-1:0] srvEn;  //  Which servos to update
	//  Outputs
	output [8*srvCnt-1:0] srvPos;  //  The required output position of all servos
	reg [8*srvCnt-1:0] srvPos;
	
	always @ (posedge clk)
	begin
		//srvPos [8*(c+1)-1:8*c] <= (srvEn [c] ? newSrvPos : srvPos [8*(c+1)-1:8*c]);
		srvPos [7:0] <= (srvEn [0] ? newSrvPos : srvPos [7:0]);
		srvPos [15:8] <= (srvEn [1] ? newSrvPos : srvPos [15:8]);
		srvPos [23:16] <= (srvEn [2] ? newSrvPos : srvPos [23:16]);
		srvPos [31:24] <= (srvEn [3] ? newSrvPos : srvPos [31:24]);
	end

	/*always @ (posedge clk)
	begin
		//  Assign the new servo position to any servo position registers
		//  Which ones are determined by srvEn
		//  Which ones are determined by srvEn
		//
		if (srvEn [0])
			srvPos [0] <= newSrvPos;
		if (srvEn [1])
			srvPos [1] <= newSrvPos;
		etc

		srvPos [0] = (srvEn [0] ? newSrvPos : srvEn[0])
		srvPos [1] = (srvEn [1] & newSrvPos)
		etc

		srvPos = (srvEn & newSrvPos)  //  Will this work with differences in bits?
		//
	end*/
	
	
	/*
	genvar i;
	generate
	   for (i=0; i < 4; i=i+1) begin : MEM
		  memory U (read, write, 
					data_in[(i*8)+7:(i*8)], 
					address,data_out[(i*8)+7:(i*8)]);
	end
	endgenerate
	*/
	
	/*
	parameter ROWBITS = 4;
	reg [ROWBITS-1:0] temp;
	
	always @(posedge sysclk) begin
		for (integer c=0; c<ROWBITS; c=c+1) begin: test
			temp[c] <= 1'b0;
		end
	end
	*/
	
	/*
	parameter ROWBITS = 4;
	reg [ROWBITS-1:0] temp;
	genvar c;
	
	always @(posedge sysclk) //Procedural context starts here
	begin
		for (c = 0; c < ROWBITS; c = c + 1) begin: test
			temp[c] <= 1'b0; //Still a genvar
		end
	end
*/

	/*reg [7:0] c;
	generate
		always @ (posedge clk)
		begin
			for (c = 0; c < srvCnt; c = c + 1'b1)
			begin
				//  Assign the new servo position to any servo position registers
				//  Which ones are determined by srvEn
				//x = 8 * (n+1) - 1
				//y = 8 * n
				srvPos [8*(c+1)-1:8*c] <= (srvEn [c] ? newSrvPos : srvPos [8*(c+1)-1:8*c]);
			end
		end
	endgenerate*/
	
	/*
	always @ (posedge clk)
	begin
		for (c=0; c<srvCnt; c=c+1)
		begin: test
			srvPos [8*(c+1)-1:8*c] <= (srvEn [c] ? newSrvPos : srvPos [8*(c+1)-1:8*c]);
		end
	end*/
	
	/*genvar c;
	always @(posedge clk) //Procedural context starts here
	begin
		for (c = 0; c < srvCnt; c = c + 1)
		begin: test
			srvPos [8*(c+1)-1:8*c] <= (srvEn [c] ? newSrvPos : srvPos [8*(c+1)-1:8*c]);
		end
	end*/
endmodule
