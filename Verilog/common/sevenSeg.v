//  Code for this file adapted from my previous code
module sevenSeg (binIn, segOut);
	//  Wrapper for modules that drive the seven-segment display
	input [7:0] binIn;
	output [27:0] segOut;
	wire [11:0] bcd;
	
	binToBCD b2B0 (.binIn (binIn), .bcdOut (bcd));
	segmentDriver sd0 (.segOut (segOut), .bcdIn (bcd));
endmodule



module binToBCD (binIn, bcdOut);
	//  Module to convert an 8-bit binary number to 3 bits of BCD (12-bits)
	//  Can be adapted for other sizes
	//  Code taken from:  http://www.eng.utah.edu/~nmcdonal/Tutorials/BCDTutorial/BCDConversion.html
	input [7:0] binIn;
	output [11:0] bcdOut;
	reg [3:0] hundreds, tens, ones;
	
	assign bcdOut = {hundreds, tens, ones};
	
	integer i;
	always @ (binIn)
	begin
		//  Set 100s, 10s, 1s to zero
		hundreds = 4'd0;
		tens = 4'd0;
		ones = 4'd0;
		
		for (i = 7; i >= 0; i = i - 1)
		begin
			//  Add 3 to columns >= 5
			if (hundreds >= 4'd5)
				hundreds = hundreds + 4'd3;
			if (tens >= 4'd5)
				tens = tens + 4'd3;
			if (ones >= 4'd5)
				ones = ones + 4'd3;
		
			//  Shift left ones
			hundreds = hundreds << 1;
			hundreds [0] = tens [3];
			tens = tens << 1;
			tens [0] = ones [3];
			ones = ones << 1;
			ones [0] = binIn [i];
		end
	end
endmodule



module segmentDriver(segOut, bcdIn);
	input [11:0] bcdIn;
	output [27:0] segOut;
	
	//  Convert BCD to 3 output lines
	segmentEncoder se0(.bcdConv(segOut[6:0]), .bcdRaw(bcdIn[3:0]));  //  Segment 0
	segmentEncoder se1(.bcdConv(segOut[13:7]), .bcdRaw(bcdIn[7:4]));  //  Segment 1
	segmentEncoder se2(.bcdConv(segOut[20:14]), .bcdRaw(bcdIn[11:8]));  //  Segment 2
	//  4th segment not used
	assign segOut [27:21] = 7'b1000000;
endmodule



module segmentEncoder(bcdConv, bcdRaw);
	input [3:0] bcdRaw;
	output [6:0] bcdConv;
	reg [6:0] bcdConv;
	
	always @ (bcdRaw)
	begin
		case (bcdRaw)
			//  LUT to switch correct lines for a given BCD value
			4'b0000: bcdConv = 7'b1000000;  //  Display 0
			4'b0001: bcdConv = 7'b1111001;  //  Display 1
			4'b0010: bcdConv = 7'b0100100;  //  Display 2
			4'b0011: bcdConv = 7'b0110000;  //  Display 3
			4'b0100: bcdConv = 7'b0011001;  //  Display 4
			4'b0101: bcdConv = 7'b0010010;  //  Display 5
			4'b0110: bcdConv = 7'b0000010;  //  Display 6
			4'b0111: bcdConv = 7'b1111000;  //  Display 7
			4'b1000: bcdConv = 7'b0000000;  //  Display 8
			4'b1001: bcdConv = 7'b0010000;  //  Display 9
			default: bcdConv = 7'b0001110;  //  Something went wrong, display an F
		endcase
	end
endmodule